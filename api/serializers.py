from rest_framework import serializers
from django.contrib.auth.models import User

from course.models import Course
from coursecomment.models import CourseComment
from courseelement.models import CourseElement
from coursestat.models import CourseStat
from usercoursestep.models import UserCourseStep


class CourseSerializer(serializers.ModelSerializer):
    class Meta:
        model = Course
        fields = '__all__'


class CourseCommentSerializer(serializers.ModelSerializer):
    class Meta:
        model = CourseComment
        fields = '__all__'


class CourseElementSerializer(serializers.ModelSerializer):
    class Meta:
        model = CourseElement
        fields = '__all__'


class CourseStatSerializer(serializers.ModelSerializer):
    class Meta:
        model = CourseStat
        fields = '__all__'


class UserCourseStepSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserCourseStep
        fields = '__all__'


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'email')
