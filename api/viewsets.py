import json

from rest_framework import viewsets
from rest_framework.permissions import AllowAny
from rest_framework.response import Response

from course.models import Course
from usercoursestep.models import UserCourseStep

from django.contrib.auth.models import User

from api.serializers import (
    CourseCommentSerializer,
    CourseElementSerializer,
    CourseStatSerializer,
    UserCourseStepSerializer,
    CourseSerializer,
    UserSerializer,
)


class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer


class CourseViewSer(viewsets.ModelViewSet):
    queryset = Course.objects.all()
    serializer_class = CourseSerializer


class CourseViewSet(viewsets.ViewSet):
    permission_classes = (AllowAny, )

    def get_queryset(self):
        queryset = Course.objects.all()
        try:
            deegree = int(self.kwargs['filter'])
            queryset.filter(deegree=deegree).select_related('')
        except ValueError:
            pass

        return queryset

    def list(self, request, filter):
        queryset = self.get_queryset()
        user = self.request.user

        context = [
            {
                course.name: {
                    'user': UserSerializer(user).data,
                    'course': CourseSerializer(course).data,
                    'percent': UserCourseStep.objects.filter(
                            user=user, course=course).last().percent,
                    'elements': CourseElementSerializer(
                        course.elements, many=True
                    ).data,
                    'comments': CourseCommentSerializer(
                        course.comments, many=True
                    ).data,
                    'stats': CourseStatSerializer(
                        course.stats, many=True
                    ).data,
                }
            } for course in queryset
        ]

        return Response(json.dumps(context))
