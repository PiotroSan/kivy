from django.db import models

from base.models import BaseMixin
from course.models import Course


class CourseComment(BaseMixin):
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)
    description = models.TextField()
    course = models.ForeignKey(
        Course,
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='comments')
