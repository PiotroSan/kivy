django==2.1.2
django-extensions==2.1.3
certifi==2018.8.24
chardet==3.0.4
idna==2.7
urllib3==1.23
djangorestframework==3.8.2