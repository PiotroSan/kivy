from django.db import models
from django.contrib.auth.models import User

from base.models import BaseMixin
from courseelement.models import CourseElement
from course.models import Course


class UserCourseStep(BaseMixin):
    user = models.ForeignKey(
        User,
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='users')
    course_element = models.ForeignKey(
        CourseElement,
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='elements')
    course = models.ForeignKey(
        Course,
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='courses')
    percent = models.DecimalField(max_digits=5, decimal_places=2, default=0)

    # @property
    # def percent(self):
    #     return (UserCourseStep.objects.filter(
    #         user=self.user, course_element=self.course_element
    #     ).count() / CourseElement.objects.filter(
    #         course=self.course_element.course
    #     ).count()) * 100 or 0

