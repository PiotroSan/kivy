from django.contrib.auth.models import User
from django.db import models

from base.models import BaseMixin


class Course(BaseMixin):
    CHARGABLE = 0
    FREE = 1
    CHOICE_AMOUNT = (
        (CHARGABLE, 'Płatny'),
        (FREE, 'Darmowy'),
    )
    DEGREE_NOTICE = 0
    DEGREE_INTERMEDIATE = 1
    DEGREE_ADVANCED = 2
    CHOICE_DEGREE = (
        (DEGREE_NOTICE, 'Podstawowy'),
        (DEGREE_INTERMEDIATE, 'Średniozaawansowany'),
        (DEGREE_ADVANCED, 'Zaawansowany'),
    )

    name = models.CharField(max_length=200, null=True, blank=True)
    description = models.TextField(null=True, blank=True)
    user = models.ManyToManyField(User)
    duration = models.TimeField(null=True, blank=True)
    amount = models.IntegerField(choices=CHOICE_AMOUNT, default=FREE)
    deegree = models.IntegerField(choices=CHOICE_DEGREE, default=DEGREE_NOTICE)
