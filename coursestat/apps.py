from django.apps import AppConfig


class CoursestatConfig(AppConfig):
    name = 'coursestat'
