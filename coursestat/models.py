from django.db import models

from base.models import BaseMixin

from course.models import Course


class CourseStat(BaseMixin):
    course = models.ForeignKey(
        Course,
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='stats')
    name = models.CharField(max_length=200)
    amount = models.IntegerField()
