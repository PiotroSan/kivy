from django.db import models

from base.models import BaseMixin

from course.models import Course


class CourseElement(BaseMixin):
    course = models.ForeignKey(
        Course,
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='elements')
    name = models.CharField(max_length=200, null=True, blank=True,)
    duration = models.TimeField(null=True, blank=True,)
